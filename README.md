# Natron docker

This repo contains an example to how to use natron with docker.

* create a natro template (test.ntp) that have input image (called MyRead) and an output (called mYWrite)
* image input is cancellino.jpg and video output is es.mp4 so run the command:
```
docker-compose run natron NatronRenderer -i MyRead /render/cancellino.jpg -w MyWrite /render/es.mp4 /render/test.ntp
# to fix permission
sudo chmod 777 es.mp4
```
